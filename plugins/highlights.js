import Vue from 'vue'
import VueHighlightJS from 'highlight.js'

Vue.use(VueHighlightJS);
