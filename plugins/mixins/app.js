import Vue from 'vue'
import { mapGetters } from 'vuex'
import Vuetify from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'

const App = {
  install (Vue, Options) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          pageSize: 'app/pageSize',
          snackbar: 'app/snackbar',
          title:'app/title',
        })
      }
    })
  }
}

Vue.use(App);
Vue.use(Vuetify, {
  iconfont: 'mdi'
});