
export const state = () => ({
  appTitle: 'Mis for Divine Youth Club Nepal',
  //user: null,
  alertMsg: null,
  alertType: 'success',
  loading: false,

})

export const mutations = {

  setAlert(state,payload){
    state.alertMsg = payload.msg;
    state.alertType = payload.type;
  },
  setAlertMsg(state, payload) {
    state.alertMsg = payload
  },
  setAlertType(state,payload){
    state.alertType = payload
  },
  setLoading(state, payload) {
    state.loading = payload
  },
  setLayout(state, payload) {
    state.layout = payload
  },

}

export const getters = {
  loading:state => {
    return state.loading
}
}

/*export const strict = false*/



