export const state = () => ({
  categories: [],
  category: {},
  meta:{
    total:0
  }
})
export const mutations = {
  setCategory(state,payload){
    state.category = payload

  },
  setCategories(state,categories){
    state.categories = categories;
  },
  setCategoryId(state,payload){
    state.category.id = payload
  },
  setMeta(state,payload){
    state.meta = payload
  }
}
export const actions = {
  fetchCategory({commit,state,rootState,dispatch},payload){

    return new Promise(((resolve, reject) => {
      commit('setLoading',true,{root:true});
      this.$axios.get('/category/'+payload)
        .then(response =>{
          commit('setLoading',false,{root:true})
          commit('setCategory',response.data.data)
          resolve(response)

        })
        .catch(error => {
          commit('setLoading',false,{root:true});
          reject(error)
        })

    }))

  },
  fetchCategories({commit,state,rootState},payload){
    return new Promise(((resolve, reject) => {
      commit('setLoading', true, {root: true});
      this.$axios.get('/category', {params: payload})
        .then(response => {
          commit('setLoading', false, {root: true})
          commit('setCategories', response.data.data)
          resolve(response);
        }).catch(error => {
          commit('setLoading', false, {root: true});
          commit('setAlert', {msg: error.response.data.errors, type: 'error'})
          reject(error);
        })
    }))
  },
  saveCategory({commit,state,rootState},payload){
    return new Promise(((resolve, reject) => {

      commit('setLoading',true,{root:true});
      const app = this;
      if(payload.id){
        this.$axios.put('/category/'+payload.id,payload)
          .then(response => {
            commit('setLoading',false,{root: true});
            resolve(response);
          })
          .catch(error => {
            commit('setLoading',false,{root:true});
            reject(error);
          });
      }
      else{
        this.$axios.post('/category',payload)
          .then(response => {
            commit('setLoading',false,{root:true});
            resolve(response);
          })
          .catch(error => {
            commit('setLoading',false,{root:true});
            reject(error);
          });
      }

    }))


  },

  deleteCategory({commit,state,rootState,dispatch},payload){
    return new Promise(((resolve, reject) => {
      commit('setLoading',true,{root:true});
      this.$axios.delete('/category/'+payload)
        .then(response =>{
          commit('setLoading',false,{root:true})
          resolve(response);

        })
        .catch(error => {
          reject(error);
        })

    }))

  },
  setCategory({commit,state},payload){
    commit('setCategory',payload);
  },
  clearCategory({commit}){
    commit('setCategory',{})
  },

}
export const getters = {
  category:state => {
    return state.category;
  },
  categories:state => {
    return state.categories;
  },
  meta:state => {
    return state.meta;
  },
  total:state => {
    return state.meta.total;
  }
}
