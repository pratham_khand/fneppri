export const getters = {
  loggedIn (state) {
    return state.loggedIn
  },
  user (state) {
    return state.user
  },
  role(state){
    return state.user.roles[0].name
  },
  isSuperAdmin(state){
    return state.user.roles[0].name === 'super_admin'
  },
  isAdmin(state){
    return state.user.roles[0].name === 'admin'
  }
}
