export const state = () => ({
  snackbar: {
    show:false,
    text: '',
    color: ''
  },
  confirm:{
    title:'Are you sure you want to delete?',
    message:'',
    option:{
      color:'primary',
      width:290
    },
    dailog:false,
    resolve: null,
    reject: null,
  },
  pageSize : [10,25,50,{"text":"$vuetify.dataIterator.rowsPerPageAll","value":-1}],
  app: {
    title: 'DreamSYS CMS',
    description: 'DreamSYS CMS is a user friendly Content Management System from Dreamsys.'
  }
})

export const mutations =  {
  setSnackbar(state,payload){
    state.snackbar.show = payload.show;
    state.snackbar.text = payload.text;
    state.snackbar.color = payload.color;

  },
  closeSnackbar(state){
    state.snackbar.show = false
  },
  setPageSize(state,payload){
    state.pageSize  = payload
  },
  openConfirm(state,payload){
    state.confirm.dialog = true
    state.confirm.title = payload.title
    state.confirm.message = payload.message
    if(payload.options) {
      Object.assign(state.confirm.options, payload.options)
    }

  },
  confirmPromise(state,payload){
    state.confirm.resolve = payload.resolve
    state.confirm.reject = payload.reject
  },
  agree(state){
    state.confirm.resolve(true)
    state.confirm.dialog = false
  },
  cancel(state){
    state.confirm.resolve(false)
    state.confirm.dialog = false
  }

}

export const actions ={
  setSnackbar({commit,state},payload){
    commit('setSnackbar',payload)
  },
  showSuccessSnackbar({commit,state},payload){
    commit('setSnackbar',{ show: 'true', text: payload, color:'green'})

  },
  showErrorSnackbar({commit,state},payload){
    commit('setSnackbar',{ show: 'true', text: payload, color:'red'})
  },
  confirm({commit},payload){
    commit('openConfirm',payload);
    return new Promise((resolve, reject) => {
      commit('confirmPromise',{resolve:resolve, reject:reject})
    })
  }
}

export const getters = {
  snackbar:state => {
    return state.snackbar;
  },
  pageSize:state => {
    return state.pageSize;
  },
  title:state => {
    return state.app.title;
  },
  confirm:state=> {
    return state.confirm;
  }
}


