import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.SITE_NAME,
    title: process.env.SITE_NAME || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.SITE_DESCRIPTION || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ,
      {
        rel: 'stylesheet',
        href:
            'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
        disabled:'true'
      },
        {
            rel: 'stylesheet',
            href:
                'https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet'
        },
        {
            rel: 'stylesheet',
            href:
                'https://fonts.googleapis.com/css?family=Cabin&display=swap" rel="stylesheet'
        },

      {
        rel: 'stylesheet',
        href:
            'https://use.fontawesome.com/releases/v5.0.13/css/all.css',
        disabled:'true'
      }
    ],
    script:[
      { src: 'https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js', async: true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/table/plugin.min.js', async:true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/advlist/plugin.min.js',async:true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/code/plugin.min.js', async: true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/emoticons/plugin.min.js', async:true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/image/plugin.min.js', async: true},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.11/plugins/preview/plugin.min.js', async:true},


    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/mixins/user.js',
    '~plugins/mixins/app.js',
    '~plugins/vee-validate.js',
    '~plugins/highlights.js',
    '~plugins/filters.js',
      '~plugins/bootstrap-vue.js',
    '~plugins/vue-social-sharing.js',
    {src: '~plugins/tinymce.js', ssr:false}
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@nuxtjs/pwa',{icon: false}],
    '@nuxtjs/vuetify',
    ['@nuxtjs/axios',{ credentials: false}],
    '@nuxtjs/auth',
    '@nuxtjs/dotenv',
    ['@nuxtjs/google-analytics', {
      id: process.env.GOOGLE_ANALYTICS || 'UA-',/*your google analatics id*/
    }],

  ],

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    theme: {
      primary: colors.blue.darken2,
      accent: colors.grey.darken3,
      secondary: colors.amber.darken3,
      info: colors.teal.lighten1,
      warning: colors.amber.base,
      error: colors.deepOrange.accent4,
      success: colors.green.accent3
    }
  },
  axios: {
    baseURL: process.env.API_URL || 'http://localhost:8000',
    proxyHeaders: false,
    credentials: false
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {url:'/login',method:'post',propertyName: 'access_token'},
          user: { url: '/auth/user', method: 'get', propertyName: 'data' },
          logout: false
        }
      }


    },
    redirect:{
      login:'/login',
      logout:false,
      home:'/admin'
    }
  },
  router: {
    middleware: ['auth']
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  buildModules:[

    ['@nuxtjs/vuetify',{
      theme: {
        themes:{
          light:{
            primary: colors.blue.darken2,
            accent: colors.grey.darken3,
            secondary: colors.amber.darken3,
            info: colors.teal.lighten1,
            warning: colors.amber.base,
            error: colors.deepOrange.accent4,
            success: colors.green.accent3
          }
        }

      }
    }],
  ],
  render: {
    http2: {
      push: true
    },
    static: {
      maxAge: '1y',
      setHeaders(res, path) {
        if (path.includes('sw.js')) {
          res.setHeader('Cache-Control', `public, max-age=${15 * 60}`)
        }
      }
    }
  }
}
